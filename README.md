## Technology Used
- .Net Core 3.1 and C#
- Swagger UI ( for API Documentation easy testing)
- Xunit and MOQ for unit tests

## Summary

Retrieves Csv file from folder, parses the file and performs operation requested
This solution uses Linq query for most of it's logic and uses CsvHelper to parse Csv files



## How to build/Run
	
	##Mac OS X / Windows
	
	- Install .net core 
		 1. Install VS Code: https://code.visualstudio.com/Download 
	     1. Install SDK: https://dotnet.microsoft.com/download/dotnet-core
		 1. Install C# for VS Code: https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp
	
	     1. This can also be done with bash `"./dotnet-install.sh --channel 3.1 --runtime aspnetcore" `
		 
	     1. Full instructions can be gotten here https://docs.microsoft.com/en-us/dotnet/core/install/macos
	 
	- Restore Packages and project dependencies and build 
		1. `dotnet restore`
		1. `dotnet build`
		
	-Run Solution
		1. `dotnet run`
	
	##Linux (CentOS 8)
	
	- Install SDK: `sudo dnf install dotnet-sdk-3.1`
		 Install runtime: `sudo dnf install aspnetcore-runtime-3.1`
	
	     Other versions of CentOS: https://docs.microsoft.com/en-us/dotnet/core/install/linux-centos
		  
	     Other Linux OS: https://docs.microsoft.com/en-us/dotnet/core/install/linux
	 
	- Restore Packages and project dependencies and build 
	    From the root folder of the project (the Folder you can see PayrollApi.sln)
		`dotnet restore`
		`dotnet build`
	
	-Run Solution
		1. `dotnet run`
		