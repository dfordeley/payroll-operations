﻿using System.Collections.Generic;

namespace MatrixOperations.Tests.TestData
{
    public static class MatrixServiceData
    {
        public static List<List<int>> InputData()
        {
            return new List<List<int>>
            {
                new List<int>{1, 2, 3},
                new List<int>{4, 5, 6},
                new List<int>{7, 8, 9}
            };
        }

        public static string FlattenedOutput()
        {
            return "1,2,3,4,5,6,7,8,9";
        }

        public static string Output()
        {
            return "1,2,3,\r\n4,5,6,\r\n7,8,9";
        }

        public static string Inverted()
        {
            return "1,4,7,\r\n2,5,8,\r\n3,6,9";
        }

    }
}
