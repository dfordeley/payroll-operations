﻿using MatrixOperation.Services.Adapters;
using MatrixOperation.Services.Implementations;
using MatrixOperations.Tests.TestData;
using NSubstitute;
using Xunit;

namespace MatrixOperations.Tests.Services
{
    public class MatrixServicesTest
    {
        private readonly MatrixService _matrixService;
        public MatrixServicesTest()
        {
            var csvReaderMock = Substitute.For<ICsvReader>();
            _matrixService = new MatrixService(csvReaderMock);
        }

        [Fact]
        public void Echo_Valid_Input_Should_Work()
        {
            var input = MatrixServiceData.InputData();
            var output = _matrixService.Echo(input);
            var expected = MatrixServiceData.Output();

            Assert.NotEmpty(output);
            Assert.Equal(output, expected);
        }

        [Fact]
        public void Echo_InValid_Input_Should_BeEmpty()
        {
            var output = _matrixService.Echo(null);
            var expected = MatrixServiceData.Output();
            Assert.True(string.IsNullOrWhiteSpace(output));
            Assert.NotEqual(output, expected);
        }
        [Fact]
        public void Flatten_Valid_Input_Should_Work()
        {
            var input = MatrixServiceData.InputData();
            var output = _matrixService.Flatten(input);
            var expected = MatrixServiceData.FlattenedOutput();

            Assert.NotEmpty(output);
            Assert.Equal(output, expected);
        }

        [Fact]
        public void Flatten_InValid_Input_Should_BeEmpty()
        {
            var output = _matrixService.Flatten(null);
            var expected = MatrixServiceData.FlattenedOutput();
            Assert.True(string.IsNullOrWhiteSpace(output));
            Assert.NotEqual(output, expected);
        }

        [Fact]
        public void Invert_Valid_Input_Should_Work()
        {
            var input = MatrixServiceData.InputData();
            var pivot = _matrixService.Pivot(input);
            var output = _matrixService.Echo(pivot);
            var expected = MatrixServiceData.Inverted();

            Assert.NotEmpty(output);
            Assert.Equal(output, expected);
        }

        [Fact]
        public void Sum_Valid_Input()
        {
            var input = MatrixServiceData.InputData();
            var output = _matrixService.Sum(input);
            var expected = "45";

            Assert.NotEmpty(output);
            Assert.Equal(output, expected);
        }

        [Fact]
        public void Multiply_Valid_Input()
        {
            var input = MatrixServiceData.InputData();
            var output = _matrixService.Multiply(input);
            var expected = "362880";

            Assert.NotEmpty(output);
            Assert.Equal(output, expected);
        }
    }
}
