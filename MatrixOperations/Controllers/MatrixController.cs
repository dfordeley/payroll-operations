﻿using System;
using System.Threading.Tasks;
using MatrixOperation.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace MatrixOperations.Controllers
{
    /// <summary>
    /// This is the entry point of the application
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MatrixController : ControllerBase
    {
        private readonly IMatrixService _matrixService;

        public MatrixController(IMatrixService matrixService)
        {
            _matrixService = matrixService;
        }

        /// <summary>
        /// Return the matrix as a string in matrix format
        /// </summary>
        /// <returns></returns>
        [Route("echo")]
        [HttpGet]
        public async Task<IActionResult> Echo()
        {
            var (parsedCsv, msg) = await _matrixService.ReadCsv<int>();

            if (!string.IsNullOrWhiteSpace(msg))
                return BadRequest(msg);

            var result = _matrixService.Echo(parsedCsv);
            return Ok(result);
        }
        /// <summary>
        /// Return the matrix as a string in matrix format where the columns and rows are inverted
        /// </summary>
        /// <returns></returns>
        [Route("invert")]
        [HttpGet]
        public async Task<IActionResult> Invert()
        {
            var (parsedCsv, msg) = await _matrixService.ReadCsv<int>();

            if (!string.IsNullOrWhiteSpace(msg))
                return BadRequest(msg);

            parsedCsv = _matrixService.Pivot(parsedCsv);

            var result = _matrixService.Echo(parsedCsv);
            return Ok(result);
        }
        /// <summary>
        /// Return the matrix as a 1 line string, with values separated by commas
        /// </summary>
        /// <returns></returns>
        [Route("flatten")]
        [HttpGet]
        public async Task<IActionResult> Flatten()
        {
            var (parsedCsv, msg) = await _matrixService.ReadCsv<int>();

            if (!string.IsNullOrWhiteSpace(msg))
                return BadRequest(msg);

            var result = _matrixService.Flatten(parsedCsv);
            return Ok(result);
        }

        /// <summary>
        /// Return the sum of the integers in the matrix
        /// </summary>
        /// <returns></returns>
        [Route("sum")]
        [HttpGet]
        public async Task<IActionResult> Sum()
        {
            var (parsedCsv, msg) = await _matrixService.ReadCsv<int>();

            if (!string.IsNullOrWhiteSpace(msg))
                return BadRequest(msg);

            var result = _matrixService.Sum(parsedCsv);
            return Ok(result);
        }

        /// <summary>
        /// Return the product of the integers in the matrix
        /// </summary>
        /// <returns></returns>
        [Route("multiply")]
        [HttpGet]
        public async Task<IActionResult> Multiply()
        {
            var (parsedCsv, msg) = await _matrixService.ReadCsv<int>();

            if (!string.IsNullOrWhiteSpace(msg))
                return BadRequest(msg);

            var result = _matrixService.Multiply(parsedCsv);
            return Ok(result);
        }

    }
}