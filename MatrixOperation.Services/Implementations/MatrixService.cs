﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MatrixOperation.Services.Adapters;
using MatrixOperation.Services.Contracts;

namespace MatrixOperation.Services.Implementations
{
    /// <summary>
    /// Implementation of our core services.
    /// All the logic uses LINQ for effective querying
    /// </summary>
    public class MatrixService : IMatrixService
    {
        private readonly ICsvReader _csvReader;

        public MatrixService(ICsvReader csvReader)
        {
            _csvReader = csvReader;
        }
        public List<List<T>> Pivot<T>(List<List<T>> input)
        {
            var maxCount = input.Max(l => l.Count);

            var result = Enumerable.Range(0, maxCount)
                .Select(index => input.Select(list => list.ElementAtOrDefault(index))
                    .ToList())
                .ToList();
            return result;
        }

        public string Flatten(List<List<int>> input)
        {
            if (input == null) return string.Empty;
            var result = new StringBuilder();
            foreach (var i in input.SelectMany(item => item))
            {
                result.Append($"{i.ToString()},");
            }
            return result.ToString().Remove(result.Length - 1);
        }

        public string Echo(List<List<int>> input)
        {
            if (input == null) return string.Empty;
            var result = new StringBuilder();
            foreach (var item in input)
            {
                var lastItem = item.Last();
                foreach (var i in item)
                {
                    _ = i == lastItem ? 
                        result.AppendLine($"{i.ToString()},") : result.Append($"{i.ToString()},");
                }
            }
            return result.ToString().Remove(result.Length - 3);
        }

        public string Multiply(List<List<int>> input)
        {
            var result = input.SelectMany(item => item)
                .Aggregate(1, (current, i) => current * i);
            return result.ToString();
        }

        public string Sum(List<List<int>> input)
        {
            var result = input.SelectMany(item => item).Sum();
            return result.ToString();
        }

        public Task<Tuple<List<List<int>>, string>> ReadCsv<T>()
        {
            return _csvReader.ReadCsv<T>();
        }
    }
}
