﻿using System;
using CsvHelper;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixOperation.Services.Adapters
{
    /// <summary>
    /// This is a wrapper class for the CsvReader library
    /// The purpose of this is to reduce dependency of an external library
    /// This being an wrapper makes it easier to swap out for another library without affecting the client
    /// </summary>
    public class CsvHelperAdapter : ICsvReader
    {
        /// <summary>
        /// These parses the csv file located in the docs folder in the root of this solution
        /// It is parsed into a List of a List
        /// This is returning the parsed data and error message if there is any
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<Tuple<List<List<int>>, string>> ReadCsv<T>()
        {
            var filePath = Directory.GetCurrentDirectory() + "\\docs\\matrix.csv";
            await using var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            using var streamReader = new StreamReader(fs, Encoding.UTF8);
            var records = new List<List<int>>();
            using var csv = new CsvReader(streamReader, CultureInfo.InvariantCulture);
            if (!await csv.ReadAsync()) return new Tuple<List<List<int>>, string>(null, "Unable to read input file");
            csv.ReadHeader();
            var headers = csv.HeaderRecord;
            records.Add(headers.Select(int.Parse).ToList());
            while (csv.Read())
            {
                var row = new List<int>();
                try
                {
                    for (var i = 0; csv.TryGetField<int>(i, out var value); i++)
                    {
                       row.Add(value);
                    }
                }
                catch (Exception)
                {
                    return new Tuple<List<List<int>>, string>(null, "Bad row data");
                }
                records.Add(row);
            }

            return records[0].Count != records.Count
                ? new Tuple<List<List<int>>, string>(null, "Invalid file: Input not a valid square matrix")
                : new Tuple<List<List<int>>, string>(records, string.Empty);
        }
    }
}
