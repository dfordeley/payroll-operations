﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatrixOperation.Services.Adapters
{
    public interface ICsvReader
    {
        Task<Tuple<List<List<int>>, string>> ReadCsv<T>();
    }
}
