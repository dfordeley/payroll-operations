﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MatrixOperation.Services.Contracts
{
    /// <summary>
    /// Services consumed by our clients.
    /// Our core functionality resides here
    /// </summary>
    public interface IMatrixService
    {
        List<List<T>> Pivot<T>(List<List<T>> input);
        string Flatten(List<List<int>> input);
        string Echo(List<List<int>> input);
        string Multiply(List<List<int>> input);
        string Sum(List<List<int>> input);
        Task<Tuple<List<List<int>>, string>> ReadCsv<T>();

    }
}
